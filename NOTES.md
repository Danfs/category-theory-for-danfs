# Notes

## Chapter 1

* Morphisms == Functions
  * Transforms something of a source group into something of a target group
* Composition is basically chaining Morphisms
  * Composition is associative ((a.b).c == (a.(b.c)))
  * The neutral element of Composition is the Identity morphism
* For a Category to be considered a Category, its morphisms need to
respect composition, and the identity morphism must exist for every element

## Chapter 2

* Types are basically sets of the values that compose them
  * Types can be finite or infinite
    * Infinite types are usually infinite due to being recursive
* Bottom value (_|_), that's new
  * Is a member of every type
  * Used to represent a non-terminating computation
    * Can also be used to represent runtime errors
  * Functions that might return bottom are called 'partial',
  otherwise they are called 'total'
* Monads mentioned 🎉
* Formal proofing mentioned 🎉
* Pure functions are ones that have deterministic output
based on their input and produce no side-effects
  * Otherwise, they are called dirty functions
* A type containing no values (an empty type),
is called Void, in Haskell.
  * In Kind, I think it was called Empty
  * In TypeScript, it is the `never` type
* A type containing only one value (a singleton type),
is called a Unit type, in Haskell, and is basically the
`void` type used to denote no inputs, or no outputs
  * In Python, we have `None` and `Ellipsis` for that
  * In TypeScript, we have `null`, `undefined` and `void`,
  all with different semantics in the language itself

## Chapter 3

* Free construction is the process of building a
category from a structure by adding to it the minimum
necessary for it to be considered a category
  * The constructed category is then called a
  _free category_
* Ordered sets can be interpreted as categories, and
we can examplify specific types of categories with them:
  * A _preorder_, an ordering with just the basic notion
  of ordering (things have an order with each other),
  is a _thin_ category: For any object pair `a, b`,
  there is at maximum one morphism linking `a` to `b`
* The set of morphisms going from an object `a` to an
object `b` in a category `C` is called a _hom-set_,
denoted as `C(a, b)` or even $Hom_C(a,b)$
  * In a thin category (such as a preorder), every
  _hom-set_ is either empty or a singleton
* Monoids mentioned 🎉
  * Monoids are just sets with a binary operation
  between its elements that: is associative;
  and has a neutral element
    * Example: Real number multiplication
