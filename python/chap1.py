from random import randint
from typing import Callable, TypeVar

T = TypeVar("T")
U = TypeVar("U")
V = TypeVar("V")

# Challenge 1
''' Implement, as best as you can, the identity function
    in your favorite language (or the second favorite,
    if your favorite language happens to be Haskell).
'''
def identity(x: T) -> T:
    return x

# Challenge 2
''' Implement the composition function in your favorite
    language. It takes two functions as arguments and
    returns a function that is their composition.
'''
def composition(
    foo: Callable[[T], U],
    bar: Callable[[U], V]
) -> Callable[[T], V]:
    return lambda x: bar(foo(x))

# Challenge 3
''' Write a program that tries to test that your
    composition function respects identity.
'''
# If I had a theorem prover, like Kind or Lean, I could literally prove
if __name__ == "__main__":
    foo: Callable[[int], int] = lambda x: x*2
    comp1: Callable[[int], int] = composition(foo, identity)
    comp2: Callable[[int], int] = composition(identity, foo)

    BIG_NUMBER_TM = 1_000_000
    # Running a lot of random tests, and pretend that this is enough
    print(
        "It is",
        all(
            foo(x) == comp1(x) and foo(x) == comp2(x)
            for _ in range(BIG_NUMBER_TM)
            if (x := randint(-BIG_NUMBER_TM, BIG_NUMBER_TM))
        ),
        "that my composition respects identity"
    )

# Challenge 4
''' Is the world-wide web a category in any sense?
    Are links morphisms?
'''
''' ...Maybe? Websites could be objects, if by "link" we mean
    "thing I click on a webpage that takes me to another webpage",
    that could be interpreted as a morphism, and we could consider
    refreshing a page as an identity morphism
'''

# Challenge 5
''' Is Facebook a category, with people as
    objects and friendships as morphisms?
'''
''' I wouldn't say so. People as objects is a good starting point,
    but friendships are not composable (A->B and B->C doesn't mean A->C),
    nor there is an identity friendship (users are not friends of themselves),
    so friendships wouldn't really feel like a morphism.
'''

# Challenge 6
''' A directed graph can be considered a category if every node has
    a connection to itself, to satisfy the identity requirement, and
    if for every pair of nodes (N, M), if I can reach M from N, there
    must be a direct connection between them, to satisfy composability.
'''
