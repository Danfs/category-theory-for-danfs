from typing import Callable, TypeVar

T = TypeVar("T")

# Challenge 1
''' Define a higher-order function (or a function object)
    `memoize` in your favorite language. This function takes
    a pure function `f` as an argument and returns a function
    that behaves almost exactly like `f`, except that it only
    calls the original function once for every argument,
    stores the result internally, and subsequently returns this
    stored result every time it's called with the same argument.
    You can tell the memoized function from the original by
    watching its performance. For instance, try to memoize a
    function that takes a long time to evaluate. You'll have to
    wait for the result the first time you call it, but on
    subsequent calls, with the same argument, you should get
    the result immediately.
'''
# I will pretend kwargs aren't a thing, just to make my life easier
def memoize(fn: Callable[..., T]) -> Callable[..., T]:
    cache: dict[tuple, T] = {}
    def inner(*args) -> T:
        key = tuple(args)
        if key not in cache:
            cache[key] = fn(*args)
        return cache[key]
    return inner

def really_long_shit(x: int) -> int:
    return x + sum(range(500000000))

def test_memoize() -> None:
    memoized_shit = memoize(really_long_shit)
    print("Testing my memoize")
    print(memoized_shit(3))
    print(memoized_shit(3))
    print(memoized_shit(3))

# Challenge 2
''' Try to memoize a function from your standard library that
    you normally use to produce random numbers. Does it work?
'''
from random import random
def test_memoize_random() -> None:
    memoized_random = memoize(random)
    print("Testing memoize on `random`")
    print(memoized_random())
    print(memoized_random())
    print(memoized_random())

''' Unsurprisingly, it doesn't work. Since the random number
    generation takes no arguments, its initial execution will
    be memoized and every subsequent call will return the same
'''

# Challenge 3
''' Most random number generators can be initialized with
    a seed. Implement a function that takes a seed, calls
    the random number generator with that seed, and returns
    the result. Memoize that function. Does it work?
'''
from random import seed
def my_random(s: int) -> float:
    seed(s)
    result = random()
    seed()
    return result

def test_memoize_my_random() -> None:
    memoized = memoize(my_random)
    print("Testing memoize on `my_random`")
    print(memoized(3))
    print(memoized(3))
    print(memoized(2))
    print(memoized(2))

''' Also doesn't work. Now the fuction has arguments, at least,
    but the first call with each seed will still be the only one
    actually random, and every other one will just pull the
    memoized one
'''

# Challenge 4
''' Which of these C++ functions are pure? Try to memoize
    them and observe what happens when you call them
    multiple times: memoized and not.
'''
''' I think I can do this without writing C++ code:
    a) Pure function;
    b) Dirty function, for it depends on user input;
    c) Dirty function, for it depends has an output side effect;
    d) Dirty function, I think? I don't remember how static
    vars work in C++, but I think they are not overwritten when
    we run the function again, so every call of this one will have
    a different value for `y` internally
'''

# Challenge 5
''' How many different functions are there from Bool to Bool?
    Can you implement them all?
'''
''' Without side effects, only 2 functions I believe?
    Identity and Negation
'''
def bool_identity(x: bool) -> bool:
    return x
def bool_negation(x: bool) -> bool:
    return not x

# Challenge 6
''' Draw a picture of a category whose only objects are
    the types Void, () (unit), and Bool; with arrows
    corresponding to all possible functions between these
    types. Label the arrows with the names of the functions.
'''
''' Aight, I'mma try my best on ASCII art for this one
    Also, every type has a identity function going from
    and to itself, but I'm not drawing those

   absurd_______Void_______absurd
        |                  |
        v       unit       v
        Unit<-----------Bool
        |                  ^
        |__________________|
              constant
(as in, it returns a constant value)
'''

if __name__ == "__main__":
    test_memoize()
    test_memoize_random()
    test_memoize_my_random()
